﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficManager : MonoBehaviour {

    public TrafficSystem trafficSystem;

    public int numberOfCars;

    private TrafficSystemPiece piece;

    public TrafficSystemVehicle car0;
    public TrafficSystemVehicle car1;
    public TrafficSystemVehicle car2;
    public TrafficSystemVehicle car3;
    public TrafficSystemVehicle car4;
    public TrafficSystemVehicle car5;
    public TrafficSystemVehicle car6;

    public List<TrafficSystemVehicle> cars;
    private List<string> car_names;

    double last;

	// Use this for initialization
	void Start ()
    {
        cars = new List<TrafficSystemVehicle>();
        car_names = new List<string>();
        
        last = Time.realtimeSinceStartup;
    }
	
	// Update is called once per frame
	void Update () 
    {
        if (Time.realtimeSinceStartup > 4)
        {
            if (cars.Count == 0)
            {
                for (int i = 0; i < numberOfCars; i++)
                {
                    TrafficSystemPiece[] pieces = GameObject.FindObjectsOfType<TrafficSystemPiece>();

                    int selected = Random.Range(0, pieces.Length);

                    piece = pieces[selected];

                    int model = Random.Range(0, 7);

                    switch (model)
                    {
                        case 0:
                            piece.SpawnRandomVehicle(car0);
                            break;
                        case 1:
                            piece.SpawnRandomVehicle(car1);
                            break;
                        case 2:
                            piece.SpawnRandomVehicle(car2);
                            break;
                        case 3:
                            piece.SpawnRandomVehicle(car3);
                            break;
                        case 4:
                            piece.SpawnRandomVehicle(car4);
                            break;
                        case 5:
                            piece.SpawnRandomVehicle(car5);
                            break;
                        case 6:
                            piece.SpawnRandomVehicle(car6);
                            break;
                    }

                    cars.Add(trafficSystem.GetSpawnedVehicles()[trafficSystem.GetSpawnedVehicles().Count - 1]);
                    car_names.Add(cars[i].name);
                }

                /*foreach (TrafficSystemVehicle car in trafficSystem.GetSpawnedVehicles())
                {
                    cars.Add(car);
                    car_names.Add(car.name);
                }*/

                Debug.Log("Number of cars: " + cars.Count);
            }
            else
            {
                if (Time.realtimeSinceStartup - last > 1)
                {
                    int index = 0;

                    foreach (TrafficSystemVehicle car in cars)
                    {
                        //Debug.Log(car);
                        if (car == null)
                        {
                            Debug.Log("Car " + index + "(" + car_names[index] + ")" + " removed.");

                            TrafficSystemPiece[] pieces = GameObject.FindObjectsOfType<TrafficSystemPiece>();

                            int selected = Random.Range(0, pieces.Length);

                            piece = pieces[selected];                            

                            //TrafficSystemPiece piece = new TrafficSystemPiece();
                            // TrafficSystemVehicleSpawner spawn = new TrafficSystemVehicleSpawner();
                            
                            switch (car_names[index])
                            {
                                case "Car0(Clone)":
                                    piece.SpawnRandomVehicle(car0);
                                    break;
                                case "Car1(Clone)":
                                    piece.SpawnRandomVehicle(car1);
                                    break;
                                case "Car2(Clone)":
                                    piece.SpawnRandomVehicle(car2);
                                    break;
                                case "Car3(Clone)":
                                    piece.SpawnRandomVehicle(car3);
                                    break;
                                case "Car4(Clone)":
                                    piece.SpawnRandomVehicle(car4);
                                    break;
                                case "Car5(Clone)":
                                    piece.SpawnRandomVehicle(car5);
                                    break;
                                case "Car6(Clone)":
                                    piece.SpawnRandomVehicle(car6);
                                    break;
                            }

                            cars[index] = trafficSystem.GetSpawnedVehicles()[trafficSystem.GetSpawnedVehicles().Count - 1];

                            Debug.Log("Car spawmn " + car_names[index] + " - Car linked " + cars[index].name);

                            /*switch (car_names[index])
                            {
                                case "Car0(Clone)":
                                    cars[index] = spawn.SpawnRandomVehicle(car0);
                                    break;
                                case "Car1(Clone)":
                                    cars[index] = spawn.SpawnRandomVehicle(car1);
                                    break;
                                case "Car2(Clone)":
                                    cars[index] = spawn.SpawnRandomVehicle(car2);
                                    break;
                                case "Car3(Clone)":
                                    cars[index] = spawn.SpawnRandomVehicle(car3);
                                    break;
                                case "Car4(Clone)":
                                    cars[index] = spawn.SpawnRandomVehicle(car4);
                                    break;
                                case "Car5(Clone)":
                                    cars[index] = spawn.SpawnRandomVehicle(car5);
                                    break;
                                case "Car6(Clone)":
                                    cars[index] = spawn.SpawnRandomVehicle(car6);
                                    break;
                            }*/
                            break;
                        }

                        index++;
                    }

                    last = Time.realtimeSinceStartup;
                }
            }
        }
   	}
}
