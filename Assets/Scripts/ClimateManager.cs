﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Excel;
using System.IO;
using System.Data;

public class CityClimate
{
    public double Tmed, Tmin, Tmax, rain;

    public CityClimate()
    {
        Tmed = 0;
        Tmin = 0;
        Tmax = 0;
        rain = 0;
    }

    public double GetTemperature(Vector3 p, int h)
    {
        double d = Vector3.Distance(p, new Vector3(0, 0, 0));

        return (Tmed + (Tmax - Tmin) / 2 * Mathf.Cos(Mathf.PI / 12 * (h - 4)) - d / 1000) * Random.RandomRange(0.9f, 1.1f);
    }
}

public class PointPollution
{
    public double[] sulfur_oxides, carbon_oxides, nitrogen_oxides, particles;

    public PointPollution()
    {
        sulfur_oxides = new double[24] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        carbon_oxides = new double[24] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        nitrogen_oxides = new double[24] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        particles = new double[24] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    }
}

public class CityPollution
{
    public PointPollution p1pollution, p2pollution, p3pollution, p4pollution, p5pollution;
    public Vector3 p1position, p2position, p3position, p4position, p5position;

    public CityPollution()
    {
        p1pollution = new PointPollution();
        p2pollution = new PointPollution();
        p3pollution = new PointPollution();
        p4pollution = new PointPollution();
        p5pollution = new PointPollution();

        p1position = new Vector3(-50.3f, 10f, 256.8f);
        p2position = new Vector3(-17.3f, 10f, -48.1f);
        p3position = new Vector3(-172f, 10f, -18f);
        p4position = new Vector3(162.5f, 10f, -6.4f);
        p5position = new Vector3(-172.1f, 10f, 12.5f);
    }

    public double GetSulfurOxides(Vector3 p, int h)
    {
        double v1 = p1pollution.sulfur_oxides[h];
        double v2 = p2pollution.sulfur_oxides[h];
        double v3 = p3pollution.sulfur_oxides[h];
        double v4 = p4pollution.sulfur_oxides[h];
        double v5 = p5pollution.sulfur_oxides[h];

        double d1 = Vector3.Distance(p1position, p);
        double d2 = Vector3.Distance(p2position, p);
        double d3 = Vector3.Distance(p3position, p);
        double d4 = Vector3.Distance(p4position, p);
        double d5 = Vector3.Distance(p5position, p);

        return (d1 * v1 + d2 * v2 + d3 * v3 + d4 * v4 + d5 * v5) / (d1 + d2 + d3 + d4 + d5) * Random.RandomRange(0.95f, 1.05f);
    }
    
    public double GetCarbonOxides(Vector3 p, int h)
    {
        double v1 = p1pollution.carbon_oxides[h];
        double v2 = p2pollution.carbon_oxides[h];
        double v3 = p3pollution.carbon_oxides[h];
        double v4 = p4pollution.carbon_oxides[h];
        double v5 = p5pollution.carbon_oxides[h];

        double d1 = Vector3.Distance(p1position, p);
        double d2 = Vector3.Distance(p2position, p);
        double d3 = Vector3.Distance(p3position, p);
        double d4 = Vector3.Distance(p4position, p);
        double d5 = Vector3.Distance(p5position, p);

        return (d1 * v1 + d2 * v2 + d3 * v3 + d4 * v4 + d5 * v5) / (d1 + d2 + d3 + d4 + d5) * Random.RandomRange(0.95f, 1.05f);
    }

    public double GetNitrogenOxides(Vector3 p, int h)
    {
        double v1 = p1pollution.nitrogen_oxides[h];
        double v2 = p2pollution.nitrogen_oxides[h];
        double v3 = p3pollution.nitrogen_oxides[h];
        double v4 = p4pollution.nitrogen_oxides[h];
        double v5 = p5pollution.nitrogen_oxides[h];

        double d1 = Vector3.Distance(p1position, p);
        double d2 = Vector3.Distance(p2position, p);
        double d3 = Vector3.Distance(p3position, p);
        double d4 = Vector3.Distance(p4position, p);
        double d5 = Vector3.Distance(p5position, p);

        return (d1 * v1 + d2 * v2 + d3 * v3 + d4 * v4 + d5 * v5) / (d1 + d2 + d3 + d4 + d5) * Random.RandomRange(0.95f, 1.05f);
    }
    
    public double GetParticles(Vector3 p, int h)
    {
        double v1 = p1pollution.particles[h];
        double v2 = p2pollution.particles[h];
        double v3 = p3pollution.particles[h];
        double v4 = p4pollution.particles[h];
        double v5 = p5pollution.particles[h];

        double d1 = Vector3.Distance(p1position, p);
        double d2 = Vector3.Distance(p2position, p);
        double d3 = Vector3.Distance(p3position, p);
        double d4 = Vector3.Distance(p4position, p);
        double d5 = Vector3.Distance(p5position, p);

        return (d1 * v1 + d2 * v2 + d3 * v3 + d4 * v4 + d5 * v5) / (d1 + d2 + d3 + d4 + d5) * Random.RandomRange(0.95f, 1.05f);
    }
}

public class ClimateManager : MonoBehaviour {

    public CityPollution pollution;
    public CityClimate climate;

    public string pBook, cBook;
    private string WorkSheet;

    private FileStream stream;
    DataSet pDataSet, cDataSet;
    private DataTable table, cTable;

    private TimeGenerator tg;

    private int day;

    private bool start;

	// Use this for initialization
	void Start () {

        tg = GameObject.Find("Time").GetComponent<TimeGenerator>();

        pollution = new CityPollution();

        climate = new CityClimate();

        stream = File.Open(Application.streamingAssetsPath + "/Climate/" + pBook, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);

        IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

        pDataSet = excelReader.AsDataSet();

        stream = File.Open(Application.streamingAssetsPath + "/Climate/" + cBook, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);

        excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

        cDataSet = excelReader.AsDataSet();

        day = tg.date.Day;
        start = true;
	}
	
	// Update is called once per frame
	void Update () {

        if ((day != tg.date.Day)||(start == true))
        {
            start = false;

            switch (tg.date.Month)
            {
                case 1:
                    WorkSheet = "Jan";
                    break;
                case 2:
                    WorkSheet = "Feb";
                    break;
                case 3:
                    WorkSheet = "Mar";
                    break;
                case 4:
                    WorkSheet = "Apr";
                    break;
                case 5:
                    WorkSheet = "May";
                    break;
                case 6:
                    WorkSheet = "Jun";
                    break;
                case 7:
                    WorkSheet = "Jul";
                    break;
                case 8:
                    WorkSheet = "Aug";
                    break;
                case 9:
                    WorkSheet = "Sep";
                    break;
                case 10:
                    WorkSheet = "Oct";
                    break;
                case 11:
                    WorkSheet = "Nov";
                    break;
                case 12:
                    WorkSheet = "Dec";
                    break;
            }
         
            table = pDataSet.Tables[WorkSheet];

            for (int i = 1; i < table.Rows.Count; i++)
            {
                if (int.Parse(table.Rows[i][4].ToString()) == tg.date.Day)
                {
                    if (int.Parse(table.Rows[i][0].ToString()) == 1)
                    {
                        // Point 1:
                        if (int.Parse(table.Rows[i][1].ToString()) == 1)
                        {
                            // Sulfur Oxides:
                            for (int j = 5; j < table.Columns.Count; j++)
                            {
                                pollution.p1pollution.sulfur_oxides[j - 5] = double.Parse(table.Rows[i][j].ToString());
                            }
                        }
                        else if (int.Parse(table.Rows[i][1].ToString()) == 6)
                        {
                            // Carbon Oxides:
                            for (int j = 5; j < table.Columns.Count; j++)
                            {
                                pollution.p1pollution.carbon_oxides[j - 5] = double.Parse(table.Rows[i][j].ToString());
                            }
                        }
                        else if (int.Parse(table.Rows[i][1].ToString()) == 7)
                        {
                            // Nitrogen Oxides:
                            for (int j = 5; j < table.Columns.Count; j++)
                            {
                                pollution.p1pollution.nitrogen_oxides[j - 5] = double.Parse(table.Rows[i][j].ToString());
                            }
                        }
                        else if (int.Parse(table.Rows[i][1].ToString()) == 10)
                        {
                            // Particles:
                            for (int j = 5; j < table.Columns.Count; j++)
                            {
                                pollution.p1pollution.particles[j - 5] = double.Parse(table.Rows[i][j].ToString());
                            }
                        }
                    }
                    else if (int.Parse(table.Rows[i][0].ToString()) == 2)
                    {
                        // Point 2:
                        if (int.Parse(table.Rows[i][1].ToString()) == 1)
                        {
                            // Sulfur Oxides:
                            for (int j = 5; j < table.Columns.Count; j++)
                            {
                                pollution.p2pollution.sulfur_oxides[j - 5] = double.Parse(table.Rows[i][j].ToString());
                            }
                        }
                        else if (int.Parse(table.Rows[i][1].ToString()) == 6)
                        {
                            // Carbon Oxides:
                            for (int j = 5; j < table.Columns.Count; j++)
                            {
                                pollution.p2pollution.carbon_oxides[j - 5] = double.Parse(table.Rows[i][j].ToString());
                            }
                        }
                        else if (int.Parse(table.Rows[i][1].ToString()) == 7)
                        {
                            // Nitrogen Oxides:
                            for (int j = 5; j < table.Columns.Count; j++)
                            {
                                pollution.p2pollution.nitrogen_oxides[j - 5] = double.Parse(table.Rows[i][j].ToString());
                            }
                        }
                        else if (int.Parse(table.Rows[i][1].ToString()) == 10)
                        {
                            // Particles:
                            for (int j = 5; j < table.Columns.Count; j++)
                            {
                                pollution.p2pollution.particles[j - 5] = double.Parse(table.Rows[i][j].ToString());
                            }
                        }
                    }
                    else if (int.Parse(table.Rows[i][0].ToString()) == 3)
                    {
                        // Point 3:
                        if (int.Parse(table.Rows[i][1].ToString()) == 1)
                        {
                            // Sulfur Oxides:
                            for (int j = 5; j < table.Columns.Count; j++)
                            {
                                pollution.p3pollution.sulfur_oxides[j - 5] = double.Parse(table.Rows[i][j].ToString());
                            }
                        }
                        else if (int.Parse(table.Rows[i][1].ToString()) == 6)
                        {
                            // Carbon Oxides:
                            for (int j = 5; j < table.Columns.Count; j++)
                            {
                                pollution.p3pollution.carbon_oxides[j - 5] = double.Parse(table.Rows[i][j].ToString());
                            }
                        }
                        else if (int.Parse(table.Rows[i][1].ToString()) == 7)
                        {
                            // Nitrogen Oxides:
                            for (int j = 5; j < table.Columns.Count; j++)
                            {
                                pollution.p3pollution.nitrogen_oxides[j - 5] = double.Parse(table.Rows[i][j].ToString());
                            }
                        }
                        else if (int.Parse(table.Rows[i][1].ToString()) == 10)
                        {
                            // Particles:
                            for (int j = 5; j < table.Columns.Count; j++)
                            {
                                pollution.p3pollution.particles[j - 5] = double.Parse(table.Rows[i][j].ToString());
                            }
                        }
                    }
                    else if (int.Parse(table.Rows[i][0].ToString()) == 4)
                    {
                        // Point 4:
                        if (int.Parse(table.Rows[i][1].ToString()) == 1)
                        {
                            // Sulfur Oxides:
                            for (int j = 5; j < table.Columns.Count; j++)
                            {
                                pollution.p4pollution.sulfur_oxides[j - 5] = double.Parse(table.Rows[i][j].ToString());
                            }
                        }
                        else if (int.Parse(table.Rows[i][1].ToString()) == 6)
                        {
                            // Carbon Oxides:
                            for (int j = 5; j < table.Columns.Count; j++)
                            {
                                pollution.p4pollution.carbon_oxides[j - 5] = double.Parse(table.Rows[i][j].ToString());
                            }
                        }
                        else if (int.Parse(table.Rows[i][1].ToString()) == 7)
                        {
                            // Nitrogen Oxides:
                            for (int j = 5; j < table.Columns.Count; j++)
                            {
                                pollution.p4pollution.nitrogen_oxides[j - 5] = double.Parse(table.Rows[i][j].ToString());
                            }
                        }
                        else if (int.Parse(table.Rows[i][1].ToString()) == 10)
                        {
                            // Particles:
                            for (int j = 5; j < table.Columns.Count; j++)
                            {
                                pollution.p4pollution.particles[j - 5] = double.Parse(table.Rows[i][j].ToString());
                            }
                        }
                    }
                    else if (int.Parse(table.Rows[i][0].ToString()) == 5)
                    {
                        // Point 5:
                        if (int.Parse(table.Rows[i][1].ToString()) == 1)
                        {
                            // Sulfur Oxides:
                            for (int j = 5; j < table.Columns.Count; j++)
                            {
                                pollution.p5pollution.sulfur_oxides[j - 5] = double.Parse(table.Rows[i][j].ToString());
                            }
                        }
                        else if (int.Parse(table.Rows[i][1].ToString()) == 6)
                        {
                            // Carbon Oxides:
                            for (int j = 5; j < table.Columns.Count; j++)
                            {
                                pollution.p5pollution.carbon_oxides[j - 5] = double.Parse(table.Rows[i][j].ToString());
                            }
                        }
                        else if (int.Parse(table.Rows[i][1].ToString()) == 7)
                        {
                            // Nitrogen Oxides:
                            for (int j = 5; j < table.Columns.Count; j++)
                            {
                                pollution.p5pollution.nitrogen_oxides[j - 5] = double.Parse(table.Rows[i][j].ToString());
                            }
                        }
                        else if (int.Parse(table.Rows[i][1].ToString()) == 10)
                        {
                            // Particles:
                            for (int j = 5; j < table.Columns.Count; j++)
                            {
                                pollution.p5pollution.particles[j - 5] = double.Parse(table.Rows[i][j].ToString());
                            }
                        }
                    }
                }
            }

            cTable = cDataSet.Tables[WorkSheet];

            for (int i = 1; i < cTable.Rows.Count; i++)
            {
                if (int.Parse(cTable.Rows[i][1].ToString()) == tg.date.Day)
                {
                    climate.Tmed = double.Parse(cTable.Rows[i][2].ToString());
                    climate.Tmax = double.Parse(cTable.Rows[i][3].ToString());
                    climate.Tmin = double.Parse(cTable.Rows[i][4].ToString());
                    climate.rain = double.Parse(cTable.Rows[i][5].ToString());

                    break;
                }
            }

            //Debug.Log("Pollution-Sulfur Oxide: " + pollution.p1pollution.sulfur_oxides[1]);
            //Debug.Log("Pollution-Carbon Oxide: " + pollution.p1pollution.carbon_oxides[1]);
            //Debug.Log("Pollution-Nitrogen Oxide: " + pollution.p1pollution.nitrogen_oxides[1]);
            //Debug.Log("Pollution-Particles: " + pollution.p1pollution.particles[1]);
            
            day = tg.date.Day;
        }
	}
}
