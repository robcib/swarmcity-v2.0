﻿using PopulationEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class PopulationManager : MonoBehaviour {

    public double frequency;

    TimeGenerator tg;

    private Stopwatch stopwatch;

	// Use this for initialization
	void Start () {

        tg = GameObject.Find("Time").GetComponent<TimeGenerator>();

        foreach (GameObject go in GameObject.FindGameObjectsWithTag("Population"))
        {
            PopulationGenerator pg = go.GetComponent<PopulationGenerator>();

            pg.enabled = false;
        }
        
        stopwatch = new Stopwatch();

        stopwatch.Start();
	}
	
	// Update is called once per frame
	void Update () {

        if (stopwatch.Elapsed.Seconds > frequency / 1000)
        {
            stopwatch.Stop();

            foreach (GameObject go in GameObject.FindGameObjectsWithTag("Population"))
            {
                PopulationGenerator pg = go.GetComponent<PopulationGenerator>();

                switch(pg.name)
                {
                    case "Stadium1":
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Sunday)||(tg.date.DayOfWeek == System.DayOfWeek.Saturday)) && ((tg.date.Hour > 17) && (tg.date.Hour < 22)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.ManualGeneration();
                            }
                        }
                        else 
                        {
                            if (pg.enabled == true)
                            {
                                pg.enabled = false;

                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                            }
                        }
                        break;

                    case "Stadium2":
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Sunday) || (tg.date.DayOfWeek == System.DayOfWeek.Saturday)) && ((tg.date.Hour > 17) && (tg.date.Hour < 22)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.ManualGeneration();
                            }
                        }
                        else
                        {
                            if (pg.enabled == true)
                            {
                                pg.enabled = false;

                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                            }
                        }
                        break;

                    case "Industry1":
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 6) && (tg.date.Hour < 10)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.ManualGeneration();
                            }
                        }
                        else
                        {
                            if (pg.enabled == true)
                            {
                                pg.enabled = false;

                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                            }
                        }
                        break;

                    case "Industry2":
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 6) && (tg.date.Hour < 10)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.ManualGeneration();
                            }
                        }
                        else
                        {
                            if (pg.enabled == true)
                            {
                                pg.enabled = false;

                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                            }
                        }
                        break;

                    case "Industry3":
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 16) && (tg.date.Hour < 20)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.ManualGeneration();
                            }
                        }
                        else
                        {
                            if (pg.enabled == true)
                            {
                                pg.enabled = false;

                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                            }
                        }
                        break;

                    case "Industry4":
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 16) && (tg.date.Hour < 20)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.ManualGeneration();
                            }
                        }
                        else
                        {
                            if (pg.enabled == true)
                            {
                                pg.enabled = false;

                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                            }
                        }
                        break;

                    case "Park1":
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 17) && (tg.date.Hour < 21)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.ManualGeneration();
                            }
                        }
                        else if (((tg.date.DayOfWeek == System.DayOfWeek.Sunday) || (tg.date.DayOfWeek == System.DayOfWeek.Saturday)) && ((tg.date.Hour > 9) && (tg.date.Hour < 21)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.ManualGeneration();
                            }
                        }
                        else
                        {
                            if (pg.enabled == true)
                            {
                                pg.enabled = false;

                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                            }
                        }
                        break;

                    case "Park2":
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 17) && (tg.date.Hour < 21)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.ManualGeneration();
                            }
                        }
                        else if (((tg.date.DayOfWeek == System.DayOfWeek.Sunday) || (tg.date.DayOfWeek == System.DayOfWeek.Saturday)) && ((tg.date.Hour > 9) && (tg.date.Hour < 21)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.ManualGeneration();
                            }
                        }
                        else
                        {
                            if (pg.enabled == true)
                            {
                                pg.enabled = false;

                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                            }
                        }
                        break;

                    case "Park3":
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 17) && (tg.date.Hour < 21)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.ManualGeneration();
                            }
                        }
                        else if (((tg.date.DayOfWeek == System.DayOfWeek.Sunday) || (tg.date.DayOfWeek == System.DayOfWeek.Saturday)) && ((tg.date.Hour > 9) && (tg.date.Hour < 21)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.ManualGeneration();
                            }
                        }
                        else
                        {
                            if (pg.enabled == true)
                            {
                                pg.enabled = false;

                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                            }
                        }
                        break;

                    case "Station1":
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 6) && (tg.date.Hour < 9)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.ManualGeneration();
                            }
                        }
                        else
                        {
                            if (pg.enabled == true)
                            {
                                pg.enabled = false;

                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                            }
                        }
                        break;

                    case "Station2":
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 19) && (tg.date.Hour < 22)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.ManualGeneration();
                            }
                        }
                        else
                        {
                            if (pg.enabled == true)
                            {
                                pg.enabled = false;

                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                            }
                        }
                        break;

                    case "Centre1":
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 13) && (tg.date.Hour < 15)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.ManualGeneration();
                            }
                        }
                        else if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 18) && (tg.date.Hour < 20)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.ManualGeneration();
                            }
                        }
                        else if (((tg.date.DayOfWeek == System.DayOfWeek.Saturday) || (tg.date.DayOfWeek == System.DayOfWeek.Sunday)) && ((tg.date.Hour > 12) && (tg.date.Hour < 20)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.ManualGeneration();
                            }
                        }
                        else                        
                        {
                            if (pg.enabled == true)
                            {
                                pg.enabled = false;

                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                            }
                        }
                        break;

                    case "Centre2":
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 13) && (tg.date.Hour < 15)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.ManualGeneration();
                            }
                        }
                        else if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 18) && (tg.date.Hour < 20)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.ManualGeneration();
                            }
                        }
                        else if (((tg.date.DayOfWeek == System.DayOfWeek.Saturday) || (tg.date.DayOfWeek == System.DayOfWeek.Sunday)) && ((tg.date.Hour > 12) && (tg.date.Hour < 20)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.ManualGeneration();
                            }
                        }
                        else                        
                        {
                            if (pg.enabled == true)
                            {
                                pg.enabled = false;

                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                            }
                        }
                        break;

                    case "Centre3":
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 13) && (tg.date.Hour < 15)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.ManualGeneration();
                            }
                        }
                        else if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 18) && (tg.date.Hour < 20)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.ManualGeneration();
                            }
                        }
                        else if (((tg.date.DayOfWeek == System.DayOfWeek.Saturday) || (tg.date.DayOfWeek == System.DayOfWeek.Sunday)) && ((tg.date.Hour > 12) && (tg.date.Hour < 20)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.ManualGeneration();
                            }
                        }
                        else                        
                        {
                            if (pg.enabled == true)
                            {
                                pg.enabled = false;

                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                            }
                        }
                        break;

                    case "Centre4":
                        if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 13) && (tg.date.Hour < 15)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.ManualGeneration();
                            }
                        }
                        else if (((tg.date.DayOfWeek == System.DayOfWeek.Monday) || (tg.date.DayOfWeek == System.DayOfWeek.Tuesday) || (tg.date.DayOfWeek == System.DayOfWeek.Wednesday) || (tg.date.DayOfWeek == System.DayOfWeek.Thursday) || (tg.date.DayOfWeek == System.DayOfWeek.Friday)) && ((tg.date.Hour > 18) && (tg.date.Hour < 20)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.ManualGeneration();
                            }
                        }
                        else if (((tg.date.DayOfWeek == System.DayOfWeek.Saturday) || (tg.date.DayOfWeek == System.DayOfWeek.Sunday)) && ((tg.date.Hour > 12) && (tg.date.Hour < 20)))
                        {
                            if (pg.enabled == false)
                            {
                                pg.enabled = true;

                                pg.ManualGeneration();
                            }
                        }
                        else                        
                        {
                            if (pg.enabled == true)
                            {
                                pg.enabled = false;

                                for (int i = 0; i < go.transform.childCount; i++)
                                {
                                    Transform child = go.transform.GetChild(i);

                                    if (child.tag == "Person")
                                    {
                                        Destroy((child as Transform).gameObject);
                                    }
                                }
                            }
                        }
                        break;
                }
            }

            stopwatch.Start();
        }
	}
}
