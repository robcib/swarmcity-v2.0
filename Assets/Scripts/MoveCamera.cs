﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour {

	public float speed = 5.0f;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update()
	{
		if((Input.GetKey(KeyCode.RightArrow))||(Input.GetKey(KeyCode.D)))
		{
			transform.position = transform.position + new Vector3(speed * Time.deltaTime,0,0);
		}
		if((Input.GetKey(KeyCode.LeftArrow))||(Input.GetKey(KeyCode.A)))
		{
			transform.position = transform.position - new Vector3(speed * Time.deltaTime,0,0);
		}
		if((Input.GetKey(KeyCode.DownArrow))||(Input.GetKey(KeyCode.S)))
		{
			transform.position = transform.position - new Vector3(0, 0, speed * Time.deltaTime);
		}
		if((Input.GetKey(KeyCode.UpArrow))||(Input.GetKey(KeyCode.W)))
		{
			transform.position = transform.position + new Vector3(0, 0, speed * Time.deltaTime);
		}
		if (Input.GetKey(KeyCode.Q)) 
		{
			transform.RotateAround (transform.position, new Vector3 (0, 1, 0), -speed * Time.deltaTime);			
		}
		if (Input.GetKey(KeyCode.E)) 
		{
			transform.RotateAround (transform.position, new Vector3 (0, 1, 0), speed * Time.deltaTime);
		}
	}
}
