﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Microsoft.Xna.Framework.Graphics;

public class PictureGenerator : MonoBehaviour {

    public Camera camera;

    public int resWidth, resHeight;

    public Vector3 Initial, Final;
    public int N_T;

    private float stepX, stepZ;

    public bool mapping;

    public Vector3 pose;

    public List<Texture2D> pictures;

    private List<int> show;

    // Use this for initialization
	void Start () {

        mapping = true;

        transform.position = Initial;

        stepX = (Final.x - Initial.x) / N_T;
        stepZ = (Final.z - Initial.z) / N_T;

        show = new List<int>(new int[] {1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0 });
        pictures = new List<Texture2D>();
	}
	
	// Update is called once per frame
	void Update () {

        if (mapping)
        {
            RenderTexture rt = new RenderTexture(resWidth, resHeight, 24);

            camera.targetTexture = rt;

            Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);

            camera.Render();

            RenderTexture.active = rt;

            screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);

            camera.targetTexture = null;

            RenderTexture.active = null;

            pictures.Add(screenShot);

            pose = transform.position;

            pose.x += stepX;

            if (pose.x >= Final.x)
            {
                pose.x = Initial.x;
                pose.z += stepZ;

                if (pose.z <= Final.z)
                {
                    pose.z = Initial.z;
                    mapping = false;

                    //Texture2D image = new Texture2D(pictures.Count * resWidth, pictures.Count * resHeight, TextureFormat.RGB24, false);

                    byte[] aux = pictures[0].EncodeToPNG();//.GetRawTextureData();

                    int size = 0;

                    for (int n = 0; n < pictures.Count; n++)
                    {
                        aux = pictures[n].EncodeToPNG();

                        size += aux.Length;
                    }

                    byte[] complete_byte = new byte[2*size];

                    for (int n = 0; n < pictures.Count; n++)
                    {
                        if (show[n] == 1)
                        {
                            byte[] bytes = pictures[n].EncodeToPNG();

                            string number;
                            if (n + 1 < 10)
                                number = "00" + (n + 1).ToString();
                            else if (n + 1 < 100)
                                number = "0" + (n + 1).ToString();
                            else
                                number = (n + 1).ToString();

                            string filename = "E:\\Pictures\\" + "Picture" + number + ".png"; //Application.dataPath + "\\Pictures" + "\\Picture" + n.ToString() + ".png";
                            System.IO.File.WriteAllBytes(filename, bytes);
                            
                            /*aux = pictures[n].EncodeToPNG();//.GetRawTextureData();

                            Debug.Log("N = " + n.ToString() + "-SIZE " + aux.Length);

                            for (int i = 0; i < aux.Length; i++)
                            {
                                //Debug.Log("N=" + n.ToString() + "i=" + i.ToString());
                                complete_byte[n * aux.Length + i] = aux[i];
                            }*/
                        }
                        else
                        {
                            byte[] bytes = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false).EncodeToPNG();

                            string number;
                            if (n + 1 < 10)
                                number = "00" + (n + 1).ToString();
                            else if (n + 1 < 100)
                                number = "0" + (n + 1).ToString();
                            else
                                number = (n + 1).ToString();

                            string filename = "E:\\Pictures\\" + "Picture" + number + ".png"; //Application.dataPath + "\\Pictures" + "\\Picture" + n.ToString() + ".png";
                            System.IO.File.WriteAllBytes(filename, bytes);
                                                        
                            /*aux = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false).EncodeToPNG();//.GetRawTextureData();
                            for (int i = 0; i < aux.Length; i++)
                            {
                                complete_byte[n * aux.Length + i] = aux[i];
                            }*/
                        }                        
                    }

                    //string filename = "E:\\Pictures\\" + "Picture" + ".png";
                    //System.IO.File.WriteAllBytes(filename, complete_byte);
                }
            }

            transform.position = pose;
        }
	}
}
