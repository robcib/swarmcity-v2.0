﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeGenerator : MonoBehaviour {

    public int mode;

    public int step;

    public int[] reference;

    public DateTime date;

	// Use this for initialization
	void Start () {    

        if (mode == 0)
        {
            date = DateTime.Now;
        }
        else
        {
            date = new DateTime(reference[0], reference[1], reference[2], reference[3], reference[4], reference[5]);
        }
	}
	
	// Update is called once per frame
	void Update () {

        date = date.Add(new TimeSpan(0, 0, step));
        if (mode == 2)
        {
            Debug.Log(date.Year + "/" + date.Month + "/" + date.Day + "-" + date.Hour + ":" + date.Minute + ":" + date.Second);
        }
	}
}
