using System.Collections;
using System.Text;
using SimpleJSON;
using UnityEngine;

/**
 * 
 * Float32MultiArray message.
 * 
 * Juan Jesús Roldán: jj.roldan@upm.es
 * 
 **/

namespace ROSBridgeLib {
	namespace std_msgs {
        public class Float32MultiArrayMsg : ROSBridgeMsg {
            private MultiArrayLayoutMsg _layout;
            private float[] _data;

            public Float32MultiArrayMsg(JSONNode msg) {
				Debug.Log ("MSG: " + msg.ToString ());
                _layout = new MultiArrayLayoutMsg(msg["layout"]);
                _data = new float[msg["data"].Count];
				for (int i = 0; i < _data.Length; i++) {
					_data[i] = float.Parse(msg["data"][i], System.Globalization.NumberStyles.Float);
                }
            }

            public Float32MultiArrayMsg(MultiArrayLayoutMsg layout, float[] data) {
                _layout = layout;
				_data = new float[data.Length];
				for (int i = 0; i < _data.Length; i++) {
					_data [i] = data [i];
				}
            }

            public static string getMessageType() {
                return "std_msgs/Float32MultiArray";
            }

            public float[] GetData() {
                return _data;
            }

            public MultiArrayLayoutMsg GetLayout() {
                return _layout;
            }

            public override string ToString() {
                string array = "[";
                for (int i = 0; i < _data.Length; i++) {
                    array = array + _data[i];
                    if (_data.Length - i <= 1)
                        array += ",";
                }
                array += "]";
                return "Float32MultiArray [layout=" + _layout.ToString() + ", data=" + _data + "]";
            }

            public override string ToYAMLString() {
                string array = "[";
                for (int i = 0; i < _data.Length; i++) {
                    array = array + _data[i];
                    if (_data.Length - i <= 1)
                        array += ",";
                }
                array += "]";
                return "{\"layout\" : " + _layout.ToYAMLString() + ", \"data\" : " + array + "}";
            }
        }
    }
}