﻿using System.Collections;
using System.Text;
using SimpleJSON;
using ROSBridgeLib.std_msgs;
using ROSBridgeLib.geometry_msgs;
using UnityEngine;

/*
 * Define a swarmcity_msgs twistarray message.
 * 
 * @author Juan Jesús Roldán (jj.roldan@upm.es)
 */

namespace ROSBridgeLib {
	namespace swarmcity_msgs {
		public class StateArrayMsg : ROSBridgeMsg {
            public float _time;
            public geometry_msgs.PoseMsg[] _poses;
            public TwistMsg[] _twists;
            public float[] _energies;

			public StateArrayMsg(JSONNode msg) {
                _time = float.Parse(msg["time"]);

                _poses = new geometry_msgs.PoseMsg[msg["poses"].Count];

                for (int i = 0; i < _poses.Length; i++)
                {
                    _poses[i] = new geometry_msgs.PoseMsg(msg["poses"][i]);
                }
                
                _twists = new TwistMsg[msg["twists"].Count];

                for (int i = 0; i < _twists.Length; i++)
                {
                    _twists[i] = new TwistMsg(msg["twists"][i]);
                }

                _energies = new float[msg["energies"].Count];

                for (int i = 0; i < _energies.Length; i++)
                {
                    _energies[i] = float.Parse(msg["energies"][i]);
                }
            }

            public StateArrayMsg(float time, geometry_msgs.PoseMsg[] poses, TwistMsg[] twists, float[] energies)
            {
                _time = time;
                _poses = poses;
                _twists = twists;
                _energies = energies;
            }
			
			public static string getMessageType() 
            {
				return "swarmcity_msgs1/StateArray";
			}

            public TwistMsg[] GetTwists() 
            {
				return _twists;
			}

            public geometry_msgs.PoseMsg[] GetPoses()
            {
                return _poses;
            }

            public float[] GetEnergies()
            {
                return _energies;
            }
			
			public override string ToString() {

                string arrayp = "[";
                for (int i = 0; i < _poses.Length; i++)
                {
                    arrayp = arrayp + _poses[i];
                    if (_poses.Length - i > 1)
                        arrayp += ",";
                }
                arrayp += "]";

                string arrayt = "[";
                for (int i = 0; i < _twists.Length; i++)
                {
                    arrayt = arrayt + _twists[i];
                    if (_twists.Length - i > 1)
                        arrayt += ",";
                }
                arrayt += "]";

                string arraye = "[";
                for (int i = 0; i < _energies.Length; i++)
                {
                    arraye = arraye + _energies[i];
                    if (_energies.Length - i > 1)
                        arraye += ",";
                }
                arraye += "]";

				return "swarmcity_msgs1/TwistArray [time=" + _time + ", poses=" + arrayp + ", twists=" + arrayt + ", energies=" + arraye + "]";
			}
			
			public override string ToYAMLString() {

                string arrayc = "[";
                for (int i = 0; i < _poses.Length; i++)
                {
                    arrayc = arrayc + _poses[i].ToYAMLString();
                    if (_poses.Length - i > 1)
                        arrayc += ",";
                }
                arrayc += "]";

                string arrayt = "[";
                for (int i = 0; i < _twists.Length; i++)
                {
                    arrayt = arrayt + _twists[i].ToYAMLString();
                    if (_twists.Length - i > 1)
                        arrayt += ",";
                }
                arrayt += "]";

                string arraye = "[";
                for (int i = 0; i < _energies.Length; i++)
                {
                    arraye = arraye + _energies[i];
                    if (_energies.Length - i > 1)
                        arraye += ",";
                }
                arraye += "]";

                return "{\"time\" : " + _time + ", \"poses\" : " + arrayc + ", \"twists\" : " + arrayt + ", \"energies\" : " + arraye + "}";
			}
		}
	}
}
