﻿using System.Collections;
using System.Text;
using SimpleJSON;
using ROSBridgeLib.std_msgs;
using ROSBridgeLib.sensor_msgs;
using UnityEngine;

/*
 * Define a swarmcity_msg SensorMsg message. 
 * 
 * @author Juan Jesús Roldán (jj.roldan@upm.es)
 */

namespace ROSBridgeLib
{
    namespace swarmcity_msgs
    {
        public class AgentProximityMsg : ROSBridgeMsg
        {
            public int[] _ID;
            public int[] _x_m;
            public int[] _y_m;
            public int[] _z_m;

            public AgentProximityMsg(JSONNode msg)
            {
                _ID = new int[msg["ID"].Count];

                for (int i = 0; i < _ID.Length; i++)
                {
                    _ID[i] = int.Parse(msg["ID"][i]);
                }

                _x_m = new int[msg["x_m"].Count];

                for (int i = 0; i < _x_m.Length; i++)
                {
                    _x_m[i] = int.Parse(msg["x_m"][i]);
                }

                _y_m = new int[msg["y_m"].Count];

                for (int i = 0; i < _y_m.Length; i++)
                {
                    _y_m[i] = int.Parse(msg["y_m"][i]);
                }

                _z_m = new int[msg["z_m"].Count];

                for (int i = 0; i < _z_m.Length; i++)
                {
                    _z_m[i] = int.Parse(msg["z_m"][i]);
                }
            }

            public AgentProximityMsg(int[] ID, int[] x_m, int[] y_m, int[] z_m)
            {
                _ID = ID;
                _x_m = x_m;
                _y_m = y_m;
                _z_m = z_m;
            }

            public static string getMessageType()
            {
                return "swarmcity_msgs1/AgentProximity";
            }

            public override string ToString()
            {
                string array = "[";
                for (int i = 0; i < _ID.Length; i++)
                {
                    int id = _ID[i];
                    array = array + id.ToString();
                    if (_ID.Length - i > 1)
                        array += ",";
                }
                array += "]";

                string arrayx = "[";
                for (int i = 0; i < _x_m.Length; i++)
                {
                    int x_m = _x_m[i];
                    arrayx = arrayx + x_m.ToString();
                    if (_x_m.Length - i > 1)
                        arrayx += ",";
                }
                arrayx += "]";

                string arrayy = "[";
                for (int i = 0; i < _y_m.Length; i++)
                {
                    int y_m = _y_m[i];
                    arrayy = arrayy + y_m.ToString();
                    if (_y_m.Length - i > 1)
                        arrayy += ",";
                }
                arrayy += "]";

                string arrayz = "[";
                for (int i = 0; i < _z_m.Length; i++)
                {
                    int z_m = _z_m[i];
                    arrayz = arrayz + z_m.ToString();
                    if (_z_m.Length - i > 1)
                        arrayz += ",";
                }
                arrayz += "]";

                return "swarmcity_msgs1/AgentProximity [ID=" + array + ",  x_m=" + arrayx + ", y_m=" + arrayy + ", z_m=" + arrayz + "]";
                //return "[ID=" + array + ",  x_m=" + arrayx + ", y_m=" + arrayy + ", z_m=" + arrayz + "]";
            }

            public override string ToYAMLString()
            {
                string array = "[";
                for (int i = 0; i < _ID.Length; i++)
                {
                    int id = _ID[i];
                    array = array + id;
                    if (_ID.Length - i > 1)
                        array += ",";
                }
                array += "]";

                string arrayx = "[";
                for (int i = 0; i < _x_m.Length; i++)
                {
                    int x_m = _x_m[i];
                    arrayx = arrayx + x_m;
                    if (_x_m.Length - i > 1)
                        arrayx += ",";
                }
                arrayx += "]";

                string arrayy = "[";
                for (int i = 0; i < _y_m.Length; i++)
                {
                    int y_m = _y_m[i];
                    arrayy = arrayy + y_m;
                    if (_y_m.Length - i > 1)
                        arrayy += ",";
                }
                arrayy += "]";

                string arrayz = "[";
                for (int i = 0; i < _z_m.Length; i++)
                {
                    int z_m = _z_m[i];
                    arrayz = arrayz + z_m;
                    if (_z_m.Length - i > 1)
                        arrayz += ",";
                }
                arrayz += "]";

                return "{\"ID\" : " + array + ", \"x_m\" : " + arrayx + ", \"y_m\" : " + arrayy + ", \"z_m\" : " + arrayz + "}";
            }
        }
    }
}
