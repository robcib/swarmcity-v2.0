﻿using UnityEngine;
using ROSBridgeLib;
using ROSBridgeLib.std_msgs;
using ROSBridgeLib.geometry_msgs;
using ROSBridgeLib.sensor_msgs;
using ROSBridgeLib.swarmcity_msgs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
//using Valve.VR.InteractionSystem;

public class Communications : MonoBehaviour {

    public int poseFrequency, sensorFrequency;

	private int time_secs, time_nsecs, time_nsecs_pose, time_nsecs_sensor;

    private int pose_ind, sensor_ind;

    private int ind;

    private ROSBridgeWebSocketConnection ros = null;

	private Stopwatch stopwatch;

    public string IP;

    void Start () {

		ros = new ROSBridgeWebSocketConnection ("ws://" + IP, 9090);

        //ros.AddSubscriber(typeof(Int32MultiArraySub));
		ros.AddSubscriber (typeof(TwistArraySub));

		ros.AddPublisher(typeof(StateArrayPub));
        ros.AddPublisher(typeof(SensorArrayPub));
        //ros.AddPublisher(typeof(Float32Pub));
	
        ros.Connect();

		time_secs = 0; 
		time_nsecs = 0;
		time_nsecs_pose = 0;
        time_nsecs_sensor = 0;

		stopwatch = new Stopwatch ();

		stopwatch.Start ();

        pose_ind = 0;
        sensor_ind = 0;

        ind = 0;
    }

    void OnApplicationQuit()
    {
        if (ros != null)
        {
            ros.Disconnect();
        }
    }

    // Update is called once per frame in Unity
    void Update()
    {
        //time_secs = Convert.ToInt32(stopwatch.Elapsed.TotalSeconds);
        //time_nsecs = Convert.ToInt32(stopwatch.Elapsed.TotalMilliseconds);

		//if (time_nsecs - time_nsecs_pose > poseFrequency) {
        //if (ind % 2 == 0)
        //{
            PublishStates();
        //}

            //time_nsecs_pose = time_nsecs;
        //}

            //ros.Render();


        //if (time_nsecs - time_nsecs_sensor > sensorFrequency) {
        //else
        //{
            PublishSensors();
        //}

        //ind++;

            //time_nsecs_sensor = time_nsecs;
        //}

        //PublishTime();

		ros.Render ();
    }

    private void PublishStates()
    {
        pose_ind++;

        HeaderMsg header = new HeaderMsg(pose_ind, new TimeMsg((int)time_secs, (int)time_nsecs), "Swarm");

        GameObject[] drones = GameObject.FindGameObjectsWithTag("Drone");

        ROSBridgeLib.geometry_msgs.PoseMsg[] poses = new ROSBridgeLib.geometry_msgs.PoseMsg[drones.Length];

        ROSBridgeLib.geometry_msgs.TwistMsg[] twists = new ROSBridgeLib.geometry_msgs.TwistMsg[drones.Length];

        float[] energies = new float[drones.Length];

        for (int i = 0; i < drones.Length; i++)
        //foreach (GameObject drone in drones)
        {
            string name = "Drone" + (i+1).ToString();

            //UnityEngine.Debug.Log("NAME:" + name);

            GameObject drone = GameObject.Find(name);

            //UnityEngine.Debug.Log(drone.name);

            Vector3 position = new Vector3();
            position = drone.transform.position;

            Quaternion rotation = new Quaternion();
            rotation = drone.transform.localRotation;

            ROSBridgeLib.geometry_msgs.PoseMsg pose = new ROSBridgeLib.geometry_msgs.PoseMsg(new PointMsg((float)position.x, (float)position.z, (float)position.y), new QuaternionMsg((float)rotation.x, (float)rotation.z, (float)rotation.y, (float)rotation.w));

            poses[i] = pose;

            Vector3 linVel = new Vector3();
            linVel = drone.GetComponent<DroneControl>().LinVel;

            Vector3 angVel = new Vector3();
            angVel = drone.GetComponent<DroneControl>().AngVel;

            ROSBridgeLib.geometry_msgs.TwistMsg twist = new ROSBridgeLib.geometry_msgs.TwistMsg(new Vector3Msg((float)linVel.x, (float)linVel.y, (float)linVel.z), new Vector3Msg((float)angVel.x, (float)angVel.y, (float)angVel.z));

            twists[i] = twist;

            energies[i] = drone.GetComponent<DroneControl>().energy;
            
            //i++;
        }

        //Array.Reverse(poses);
        //Array.Reverse(twists);

        StateArrayMsg stateArray = new StateArrayMsg(Time.realtimeSinceStartup, poses, twists, energies);

        ros.Publish(StateArrayPub.GetMessageTopic(), stateArray);

        //UnityEngine.Debug.Log("Pose msg sent!");
    }

    private void PublishSensors()
    {
        sensor_ind++;
        
        HeaderMsg header = new HeaderMsg(sensor_ind, new TimeMsg((int)time_secs, (int)time_nsecs), "Swarm");

        GameObject[] drones = GameObject.FindGameObjectsWithTag("Drone");

        SensorMsg[] sensors = new SensorMsg[drones.Length];
        
        for (int i = 0; i < drones.Length; i++)
        //foreach (GameObject drone in drones)
        {
            string name = "Drone" + (i+1).ToString();

//            UnityEngine.Debug.Log("NAME:" + name);

            GameObject drone = GameObject.Find(name);

  //          UnityEngine.Debug.Log(drone.name);
            DronePayload dronePayload = drone.GetComponent<DronePayload>();

            LaserScan l = dronePayload.laserscan;

            LaserScanMsg laserscan = new LaserScanMsg(header, l._angle_min, l._angle_max, l._angle_increment, l._time_increment, l._scan_time, l._range_min, l._range_max, l._ranges, l._intensities);

            //UnityEngine.Debug.Log(laserscan);

            List<Detection> d = dronePayload.detections;

            DetectionMsg[] detections = new DetectionMsg[d.Count];

            for (int j = 0; j < d.Count; j++)
            {
                PointMsg point = new PointMsg(d[j]._position.x, d[j]._position.y, 0.0f);

                Quaternion q = Quaternion.identity;

                //UnityEngine.Debug.Log(d[j]._angle);

                q.eulerAngles = new Vector3(0, 0, (float)d[j]._angle);// * (float)180.0f / (float)Math.PI);

                QuaternionMsg quaternion = new QuaternionMsg(q.x, q.y, q.z, q.w);

                detections[j] = new DetectionMsg(d[j]._type, d[j]._features, new ROSBridgeLib.geometry_msgs.PoseMsg(point, quaternion));

                //UnityEngine.Debug.Log(detections[j]);
            }

            ClimateMsg climate = new ClimateMsg((float)dronePayload.temperature, 0.0f, (float)dronePayload.sulfur_oxides, (float)dronePayload.carbon_oxides, (float)dronePayload.nitrogen_oxides, (float)dronePayload.particles);

            AgentProximityMsg[] agentproximity = new AgentProximityMsg[drones.Length];
            
            for (int j = 0; j < agentproximity.Length; j++)
            {
                agentproximity[j] = new AgentProximityMsg(new int[drones.Length], new int[drones.Length], new int[drones.Length], new int[drones.Length]);
            }

            SensorMsg sensor = new SensorMsg(laserscan, detections, climate, agentproximity);

            sensors[i] = sensor;

            //i++;
        }

        //Array.Reverse(sensors);

        SensorArrayMsg sensorArray = new SensorArrayMsg(Time.realtimeSinceStartup, sensors);

        //UnityEngine.Debug.Log("MSG: " + sensorArray.ToString());

        ros.Publish(SensorArrayPub.GetMessageTopic(), sensorArray);

        //UnityEngine.Debug.Log(sensorArray);


        //UnityEngine.Debug.Log("Sensor msg sent!");
    }

    private void PublishTime()
    {
        Float32Msg time = new Float32Msg(Time.realtimeSinceStartup);

        ros.Publish(Float32Pub.GetMessageTopic(), time);
    }
}